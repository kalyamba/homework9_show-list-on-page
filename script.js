function createList(array, parent = document.body) {
    const ul = document.createElement('ul');
    parent.appendChild(ul);

    array.forEach(item => {
        const li = document.createElement('li');
        ul.appendChild(li);

        if (Array.isArray(item)) {
            createList(item, li);
        } else {
            li.textContent = item;
        }
    });
}

function countdown(seconds) {
    const timer = document.createElement('div');
    timer.style.fontSize = '24px';
    document.body.appendChild(timer);

    let count = seconds;

    const intervalId = setInterval(() => {
        timer.textContent = `Очищення через  ${count} сек.`;
        count--;

        if (count < 0) {
            clearInterval(intervalId);
            document.body.innerHTML = '';
        }
    }, 1000);
}

const array = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];
createList(array);

countdown(3);
